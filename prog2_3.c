#include <stdio.h>
#include <string.h>


#define MAX_LENGTH 65

int check_tokens(char *str);
void print_types(char *str);


int main(int argn, char **argv) {    
  
    printf("Assignment #2-3, Nathan Huse, fujitechs416@gmail.com\n"); 

    int valid = 0;

	char input[MAX_LENGTH]; 


	while (!valid) {
		printf("> ");
		fgets(input, MAX_LENGTH, stdin);

		valid = check_tokens(input);
	}

	print_types(input);

	

	return 0; 
}


int check_tokens(char *str) {
	int count = 0;
	int i = 0;

	if (str[0] == '\n'){
		printf("ERROR! Incorrect number of tokens found.\n");
		return 0;
	}


	while (count <= 2)
	{	
		//exit if we find end of user input
		if (str[i] == '\n') { 
			return 1; 		

	//check for space
	} else if (str[i] == ' ') { 
		i++;
		//ignore multiple spaces
		while (str[i] == ' ')   	
			i++; 
	

	// anything else must be token
	} else {
		count++;

		while (str[i] != ' ' && str[i] != '\n') 
			i++;
		

	}
	
}

	printf("ERROR! Incorrect number of tokens found.\n");
	return 0;

}


void print_types(char *str) 
{
	for (int i = 0; str[i] != 0; i++) 
	{

		//exit if we find end of user input
		if (str[i] == '\n') { 
			return;   
		

		//check if we encounter space
		} else if (str[i] == ' ') { 

			//ignore multiple spaces 
			while (str[i] == ' ') 	
				i++; 

			//backup one to account for 'for loop' i++	
			i--; 
		 

		 //check if char is digit
		} else if (str[i] >= '0' && str[i] <= '9' ) 

		{  

			while (str[i] >= '0' && str[i] <= '9' ) {
				i++;
				if (str[i] == ' ' || str[i] == '\n') { 
					// if we hit "space" or "\n", entire segment is int
					printf("INT "); 
					break; 
				}

				if (str[i] < '0' || str[i] > '9') { 
					// if we hit any non-int before space, entire segment is str
					printf("STR ");
					while (str[i] != ' ' && str[i] != '\n') 
						i++; 
					break; 
			} 
		
		}
		//anything else must be string
		} else {
		
			printf("STR ");
			//push us to next delimiter
			while (str[i] != ' ' && str[i] != '\n') 
				i++; 
		}
	}

	printf("\n");
}