#include <stdio.h>
#include <string.h>

#define MAX_LENGTH 65

void print_types(char *str);


int main(int argn, char **argv) {    
    
    printf("Assignment #2-2, Nathan Huse, fujitechs416@gmail.com\n"); // header

    // setup input as char with MAX_LENGTH as size.
	char input[MAX_LENGTH]; 

	printf("> ");

	fgets(input, MAX_LENGTH, stdin);

	print_types(input);
	

	return 0;

}


void print_types(char *str) 
{
	for (int i = 0; str[i] != 0; i++) 
	{

		//exit if we find end of user input
		if (str[i] == '\n') { 
			return;   
		

		//check if we encounter space
		} else if (str[i] == ' ') { 

			//ignore multiple spaces 
			while (str[i] == ' ') 	
				i++; 

			//backup one to account for 'for loop' i++	
			i--; 
		 

		 //check if char is digit
		} else if (str[i] >= '0' && str[i] <= '9' ) 

		{  

			while (str[i] >= '0' && str[i] <= '9' ) {
				i++;
				if (str[i] == ' ' || str[i] == '\n') { 
					// if we hit "space" or "\n", entire segment is int
					printf("INT "); 
					break; 
				}
				
				if (str[i] < '0' || str[i] > '9') { 
					// if we hit any non-int before space, entire segment is str
					printf("STR ");
					while (str[i] != ' ' && str[i] != '\n') 
						i++; 
					break; 
			} 
		
		}
		//anything else must be string
		} else {
		
			printf("STR ");
			//push us to next delimiter
			while (str[i] != ' ' && str[i] != '\n') 
				i++; 
		}
	}

	printf("\n");
}

