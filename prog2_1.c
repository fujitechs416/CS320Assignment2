#include <stdio.h>
#include <string.h>


int main() {    
    int MAX_LENGTH = 65;


    printf("Assignment #2-1, Nathan Huse, fujitechs416@gmail.com\n"); // header

	// setup userChars as char with MAX_LENGTH as size.
	char userChars[MAX_LENGTH]; 
	fgets(userChars, MAX_LENGTH, stdin);
	//replace newline with null termination
	userChars[strcspn(userChars, "\n\0")] = '\0';

	char* token = strtok(userChars," ");

	while (token != NULL) {
		printf("=%s=\n", token);

		token = strtok(NULL, " ");
	}

return 0;


}
