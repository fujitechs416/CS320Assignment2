Assignment 2 is a collection of programs that manipulate and interprete character arrays to determine type of user input.

----
prog2_1.c - C program that accepts input, using space (" ") as a delimiter, and outputs the input on seperate lines surrounded by "="

prog2_2.c - C program that accepts input, using space (" ") as a delimiter, that determines type of user input (Either string or int)

prog2_3.c - Same as program 2, except checks if user has input at most 2 tokens, and loops until input is valid.

prog2_4.c - Program 4-6 uses a different algorithim that works more easily with the increasing complexity. Outputs the same as program 3, except checks for sentinel "quit" value, 
and checks if input exceeds 20 characters.

prog2_5.c - Same as program 4, except this checks if the user has entered a STR if 1 token inputted and a STR and INT if two tokens are inputted. Checks if input exceeds 65 characters.

prog2_6.c - Same as program 5, but this takes single integer command argument to determine number of times to check for input and interpret input.

----

Programs 1 - 5 can be executed in an linux distribution by typing gcc [program_name], along with ./a.out

Program 6 can be executed in an linux distribution by typing gcc [program_name], along with ./a.out [number_of_loops_int]