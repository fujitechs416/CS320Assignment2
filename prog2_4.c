#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int scrub_input(char *str);
void print_tokens(char *str);



int main(int argn, char **argv) {    
  
    printf("Assignment #2-4, Nathan Huse, fujitechs416@gmail.com\n");

    char str[256];

    while (1<2) {
    	printf("> ");

    	fgets(str, sizeof(str), stdin);

    	fflush(stdin);
    	str[strcspn(str, "\n\0")] = '\0';

    	if(!strcasecmp(str, "quit"))
    		exit(0);

    	// check if length is greater than 20
    	int length = strlen(str);
    	if (length > 20) {
    		printf("ERROR! Input string too long.\n");
    		continue;
    	}

    	int numtokens = scrub_input(str);
    	if (numtokens < 1 || numtokens > 2) {
    		printf("ERROR! Incorrect number of tokens found.\n");
    		continue;
    	}
    	print_tokens(str);
    } 

return 0;

}

// removes extra whitespace to easily interpret input.
// destroys original char array

int scrub_input(char *str)
{
	// starting index, points at first letter
	int start = 0;
	// stopping index, points at last letter
	int stop = strlen(str)-1;

	while(isspace(str[start]))
		start++;

	while(isspace(str[stop]))
		stop--;

	// if we don't find any letters, return
	if (start > stop)
		return 0;

	int numtokens = 1;

	int copy = start;
	int paste = 0;

	while (copy <= stop) {
		char c = str[copy];
		if (isspace(c)) {
			str[paste++] = ' ';
			numtokens++;

			while(isspace(str[copy]))
				copy++;


		} else {
			str[paste++] = str[copy++];
		}
	}
	str[paste] = '\0';

	return numtokens;
}


void print_tokens(char *str)
{
	// i: current pos in the string
	// start: start of the current token
	// len: gives us length
	int i, start, len;

	for (i = start = 0, len = strlen(str); i < len; i++) {

		// find the next space (or the end of the string)
		int isnumber = 1;
		while (str[i] != '\0' && !isspace(str[i])) {
			
			// found non-digit - must be a string
			if (!isdigit(str[i])) {
				isnumber = 0;
			}
			i++;
		}
		
		if (isnumber)
			printf("INT ");
		else
			printf("STR ");

		// move 'start' up to the start of the next token
		start = i + 1;
	}
	printf("\n");
}
